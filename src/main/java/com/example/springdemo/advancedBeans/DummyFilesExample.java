package com.example.springdemo.advancedBeans;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Configuration
public class DummyFilesExample {

    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void fileReadWriteExample() {

        try {
            //pt a citi dintr-un fisier avem nevoie de clasa fileReader
            FileReader reader = new FileReader ("src/main/resources/read.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);  //( new FileReader("read.txt"));


            String firstLine = bufferedReader.readLine();

            System.out.println(firstLine);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
