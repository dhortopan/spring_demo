package com.example.springdemo.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class Scheduler {

    //Bean
    @Autowired
    PrettyPrinter pp;

        //var 1
//    @Autowired
//    NotSoPrettyPrinter nspp;


    // var 2 cu constructor
    private NotSoPrettyPrinter nspp;


    public Scheduler (NotSoPrettyPrinter nspp) {
        this.nspp = nspp;
    }

    private UglyPrinter up;

    @Autowired
    public void setUp(UglyPrinter up) {
        this.up = up;
    }


    //fixed delay is in seconds
    @Scheduled(fixedDelay = 60 * 10 * 1000)
    public void every10Minutes() {
//        System.out.println("Am ajuns aici");
        pp.prettyPrinter("Am ajuns aici");
        nspp.notSoPrettyPrint("ceva ceva");
        up.uglyPrint("some ugly message");
    }


}
