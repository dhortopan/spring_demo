package com.example.springdemo.beans;


import org.springframework.stereotype.Component;

@Component
public class PrettyPrinter {

    public PrettyPrinter() {}
    public void prettyPrinter(String message) {
        System.out.println("----------");
        System.out.println("|" + message + "|");
        System.out.println("----------");
    }
}
