package com.example.springdemo.beans;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
public class UglyPrinter {

    public void uglyPrint(String message) {
        System.out.println("from ugly printer:" + message);
    }
}
