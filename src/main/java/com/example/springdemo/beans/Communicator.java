package com.example.springdemo.beans;

import org.springframework.beans.factory.annotation.Autowired;

public class Communicator {

    @Autowired
    PrettyPrinter pp;

    public void communicator (String message) {
        System.out.println("I'm sending the message");
        pp.prettyPrinter(message);
    }
}
