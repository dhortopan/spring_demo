package com.example.springdemo.beans;

import com.example.springdemo.SpringDemoApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrettyCommunicator {

    public static void main (String[] args) {
        PrettyCommunicator p1 = new PrettyCommunicator();
        p1.prettyPrint("Hello");

SpringApplication.run (SpringDemoApplication.class, args);

    }

    private void prettyPrint(String message) {
        System.out.println("-----");
        System.out.println("|" + message + "|");
        System.out.println("-----");
    }
    }



