package com.example.springdemo.ex2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
//@Configuration
//@EnableScheduling
public class Schedule {

    @Autowired
    ReaderOfFile readerOfFile;

    @Autowired
    ReaderOfKeyboard readerOfKeyboard;

    @Autowired
    GenericReader genericReader;

    @Scheduled (fixedDelay = 1000*60*10)
    public void readSomething () throws IOException {
        String a = readerOfFile.readLine();
        String b = readerOfKeyboard.readLine();

        System.out.println(a);
        System.out.println(b);
    }


}
