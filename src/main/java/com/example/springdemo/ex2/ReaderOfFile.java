package com.example.springdemo.ex2;

import org.springframework.stereotype.Component;

import javax.annotation.processing.Filer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


@Component()
public class ReaderOfFile  {

    public ReaderOfFile() {

    }

    public String readLine () throws IOException {
        FileReader fr = new FileReader("src/main/resources/read.txt");
        BufferedReader br = new BufferedReader(fr);

        String firstLine =br.readLine();
        return firstLine;

    }
}
