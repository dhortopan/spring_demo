package com.example.springdemo.ex2;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class ReaderOfKeyboard {
    Scanner s = new Scanner (System.in);

    public String readLine() {
       String liniaCitita= s.nextLine();
        return liniaCitita;
    }

    public void read() {

    }

}
