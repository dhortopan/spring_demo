package com.example.springdemo.ex1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
@SpringBootApplication
@EnableScheduling

// makes possible to schedule methods to run at certain times
public class Application {

        // aici porneste toata aplicatia
        public static void main(String[] args) {
            SpringApplication.run(com.example.springdemo.beans.Ro36Application.class, args);


        }
    }

